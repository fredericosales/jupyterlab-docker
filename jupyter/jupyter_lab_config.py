# Configuration file for lab.

# -----------------------------------------------------------------------------
# Application(SingletonConfigurable) configuration
# -----------------------------------------------------------------------------

c.Application.log_datefmt = '%d-%m-%Y %H:%M:%S'
c.Application.log_format = '[%(name)s]%(highlevel)s %(message)s'
c.Application.log_level = 30

# -----------------------------------------------------------------------------
# JupyterApp(Application) configuration
# -----------------------------------------------------------------------------

# c.JupyterApp.answer_yes = False
# c.JupyterApp.config_file = ''
# c.JupyterApp.config_file_name = ''
# c.JupyterApp.generate_config = False

# -----------------------------------------------------------------------------
# ExtensionApp(JupyterApp) configuration
# -----------------------------------------------------------------------------

# c.ExtensionApp.default_url = ''
# c.ExtensionApp.handlers = []
# c.ExtensionApp.open_browser = False
# c.ExtensionApp.settings = {}
# c.ExtensionApp.static_paths = []
# c.ExtensionApp.static_url_prefix = ''
# c.ExtensionApp.template_paths = []

# -----------------------------------------------------------------------------
# LabServerApp(ExtensionAppJinjaMixin,LabConfig,ExtensionApp) configuration
# -----------------------------------------------------------------------------

# c.LabServerApp.allowed_extensions_uris = ''
# c.LabServerApp.blacklist_uris = ''
# c.LabServerApp.blocked_extensions_uris = ''
# c.LabServerApp.listings_request_options = {}
# c.LabServerApp.whitelist_uris = ''

# ----------------------------------------------------------------------------
# LabApp(NBClassicConfigShimMixin,LabServerApp) configuration
# ----------------------------------------------------------------------------

# c.LabApp.app_dir = None
# c.LabApp.core_mode = False
# c.LabApp.default_url = '/lab'
# c.LabApp.dev_mode = False
# c.LabApp.expose_app_in_browser = False
# c.LabApp.extensions_in_dev_mode = False
# c.LabApp.override_static_url = ''
# c.LabApp.override_theme_url = ''
c.LabApp.user_settings_dir = '/home/science/.jupyter/lab/user-settings'
# c.LabApp.watch = False
c.LabApp.workspaces_dir = '/home/science/.jupyter/lab/workspaces'

# -----------------------------------------------------------------------------
# ServerApp(JupyterApp) configuration
# -----------------------------------------------------------------------------

# c.ServerApp.allow_credentials = False
c.ServerApp.allow_origin = '*'
# c.ServerApp.allow_origin_pat = ''
# c.ServerApp.allow_password_change = True
c.ServerApp.allow_remote_access = True
# c.ServerApp.allow_root = False
# c.ServerApp.authenticate_prometheus = True
# c.ServerApp.autoreload = False
# c.ServerApp.base_url = '/'
# c.ServerApp.browser = ''
# c.ServerApp.certfile = ''
# c.ServerApp.client_ca = ''
# c.ServerApp.config_manager_class = 'jupyter_server.services.config.manager.ConfigManager'
# c.ServerApp.contents_manager_class = 'jupyter_server.services.contents.largefilemanager.LargeFileManager'
# c.ServerApp.cookie_options = {}
# c.ServerApp.cookie_secret = b''
# c.ServerApp.cookie_secret_file = ''
# c.ServerApp.custom_display_url = ''
# c.ServerApp.default_url = '/'
# c.ServerApp.extra_services = []
# c.ServerApp.extra_static_paths = []
# c.ServerApp.extra_template_paths = []
# c.ServerApp.file_to_run = ''
# c.ServerApp.file_url_prefix = 'notebooks'
# c.ServerApp.get_secure_cookie_kwargs = {}
# c.ServerApp.iopub_msg_rate_limit = 1000
c.ServerApp.ip = '*'
# c.ServerApp.jinja_environment_options = {}
# c.ServerApp.jinja_template_vars = {}
# c.ServerApp.jpserver_extensions = {}
# c.ServerApp.kernel_manager_class = 'jupyter_server.services.kernels.kernelmanager.AsyncMappingKernelManager'
# c.ServerApp.kernel_spec_manager_class = 'jupyter_client.kernelspec.KernelSpecManager'
# c.ServerApp.keyfile = ''
# c.ServerApp.local_hostnames = ['localhost']
# c.ServerApp.login_handler_class = 'jupyter_server.auth.login.LoginHandler'
# c.ServerApp.logout_handler_class = 'jupyter_server.auth.logout.LogoutHandler'
# c.ServerApp.max_body_size = 536870912
# c.ServerApp.max_buffer_size = 536870912
# c.ServerApp.min_open_files_limit = 0
# c.ServerApp.notebook_dir = ''
c.ServerApp.open_browser = False
c.ServerApp.password = u'sha1:da1f80a5d957:9c0beb7d9d59adc907079a787796e0c809a28c82'
c.ServerApp.password_required = False
c.ServerApp.port = 9191
# c.ServerApp.port_retries = 50
# c.ServerApp.pylab = 'disabled'
# c.ServerApp.quit_button = True
# c.ServerApp.rate_limit_window = 3
# c.ServerApp.reraise_server_extension_failures = False
# c.ServerApp.root_dir = ''
# c.ServerApp.session_manager_class = 'jupyter_server.services.sessions.sessionmanager.SessionManager'
# c.ServerApp.shutdown_no_activity_timeout = 0
# c.ServerApp.ssl_options = {}
# c.ServerApp.terminado_settings = {}
# c.ServerApp.terminals_enabled = True
# c.ServerApp.token = '<generated>'
# c.ServerApp.trust_xheaders = False
# c.ServerApp.use_redirect_file = True
# c.ServerApp.webbrowser_open_new = 2
# c.ServerApp.websocket_compression_options = None
# c.ServerApp.websocket_url = ''
