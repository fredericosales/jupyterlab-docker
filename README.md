# Jupyterlab as docker service
---

### Creating a jupyter config file

```bash

[user@hostname] $: jupyter-lab --generate-config

```


### Setting the server 

```bash

[user@hostname ~/] $: jupyter-lab password
Enter password: ****
Verify password: ****
[NotebookPasswordApp] Wrote hashed password to /Users/you/.jupyter/jupyter_notebook_config.json


```
### Preparing a hased password

```bash

[user@hostname] $: python3
In [1]: from notebook.auth import passwd
In [2]: passwd()
Enter password: ****
Verify password: ****
Out [2]: 'sha1:67c9e60bb8b6:9ffede0825894254b2e042ea597d771089e11aed'

```

### Adding hashed password to you notebook config file

You can then add the hashed password to your jupyter_notebook_config.py. The
default location for this file jupyter_notebook_config.py is in your Jupyter
folder in your home directory, ~/.jupyter, e.g.:

    c.NotebookApp.password = u'sha1:67c9e60bb8b6:9ffede0825894254b2e042ea597d771089e11aed'

### Using SSL for encrypted communication



When using a password, it is a good idea to also use SSL with a web
certificate, so that your hashed password is not sent unencrypted by your
browser.

* Generate a self-signed certificate with OpenSSL cert and key.

```bash

[user@hostname] $: openssl req -x509 -nodes -days 730 -newkey rsa:2048 -keyout jupyter.key -out jupyter.pem

```


* Start  the notebook via secure protocol by setting the certfile to you
  self-signed certificate. 

```bash

[user@hostname] $: jupyter-lab --certifile=/absolute/path/to/your/certificate.pem --keyfile certificate.key

```


### Running the service

```bash

[user@hostname ~/] $: docker-compose up

```

## License
---

Copyright <2021> <Frederico Sales>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
