FROM debian:buster

RUN apt update && apt upgrade -y && apt -f install
RUN apt install -y locales && rm -rf /var/lib/apt/lists/* \ && localdef -i en_US
                                                          -c -f UTF-8 -A
                                                          /usr/share/locale/locale.alias
                                                          en_US.UTF-8

RUN apt install -y python3-dev python3-pip build-essential bc cmake clang
RUN apt install -y automake pkg-config sudo vim-nox && apt -f install
RUN apt install -y make libssl-dev zlib1g-dev libbz2-dev libreadline-dev 
RUN apt install -y libsqlite3-dev wget curl llvm libncurses5-dev
RUN apt install -y libncursesw5-dev xz-utils tk-dev && apt -f install
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt install -y nodejs
RUN mkdir -p $HOME/.config/systemd/user
RUN pip install --upgrade pip && pip install --upgrade setuptools

ENV EDITOR="vim"
ENV LANG eng_US.utf-8
ENV NVM_DIR="$HOME/.nvm"
ENV hostname="jupyter"

USER science
WORKDIR /home/science

COPY jupyter /home/science/.jupyter

RUN pip install -r /home/science/.jupyter/requirements.txt
RUN jupyter labextension install jupyterlab-plotwidget@4.14.3
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager plotlywidget@4.14.3

EXPOSE 9191

RUN ["jupyter-lab", "--ip=0.0.0.0", "--port=9191", "--no-browser"]
